<?php
declare(strict_types=1);
/*
 * Initial migration to create the database schema and some agent test data
 */

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20210618110300
 * @package DoctrineMigrations
 */
final class Version20210618110300 extends AbstractMigration
{
    /**
     * @return string
     */
    public function getDescription(): string
    {
        return 'Initial Database Schema';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE agent (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE agent_property (agent_id INT NOT NULL, property_id INT NOT NULL, INDEX IDX_BD23054A3414710B (agent_id), INDEX IDX_BD23054A549213EC (property_id), PRIMARY KEY(agent_id, property_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE property (id INT AUTO_INCREMENT NOT NULL, uuid VARCHAR(50) NOT NULL, property_id INT NOT NULL, description TEXT NOT NULL, town VARCHAR(25) NOT NULL, country VARCHAR(45) NOT NULL, property_type INT NOT NULL, number_of_bedrooms INT NOT NULL, number_of_bathrooms INT NOT NULL, price INT NOT NULL, latitude VARCHAR(13) NOT NULL, longitude VARCHAR(13) NOT NULL, for_sale_or_rent VARCHAR(5) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE agent_property ADD CONSTRAINT FK_BD23054A3414710B FOREIGN KEY (agent_id) REFERENCES agent (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE agent_property ADD CONSTRAINT FK_BD23054A549213EC FOREIGN KEY (property_id) REFERENCES property (id) ON DELETE CASCADE');

        $this->populateAgentData();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE agent_property DROP FOREIGN KEY FK_BD23054A3414710B');
        $this->addSql('ALTER TABLE agent_property DROP FOREIGN KEY FK_BD23054A549213EC');
        $this->addSql('DROP TABLE agent');
        $this->addSql('DROP TABLE agent_property');
        $this->addSql('DROP TABLE property');
    }

    /**
     * Dummy agent data for test purposes
     */
    private function populateAgentData(): void
    {
        $this->addSql("INSERT INTO agent VALUES (NULL, 'Agent 1')");
        $this->addSql("INSERT INTO agent VALUES (NULL, 'Agent 2')");
        $this->addSql("INSERT INTO agent VALUES (NULL, 'Agent 3')");
        $this->addSql("INSERT INTO agent VALUES (NULL, 'Agent 4')");
        $this->addSql("INSERT INTO agent VALUES (NULL, 'Agent 5')");
        $this->addSql("INSERT INTO agent VALUES (NULL, 'Agent 6')");
    }
}

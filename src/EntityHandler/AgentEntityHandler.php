<?php
declare(strict_types=1);
/**
 * Entity handler for Agent entity
 */

namespace App\EntityHandler;

use App\Entity\Agent;
use App\Entity\Property;
use App\Repository\AgentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

/**
 * Class AgentEntityHandler
 * @package App\EntityHandler
 */
class AgentEntityHandler
{
    /** @var EntityManagerInterface $entityManager */
    private EntityManagerInterface $entityManager;

    /** @var AgentRepository $agentRepository */
    private AgentRepository $agentRepository;

    /** @var PropertyEntityHandler $propertyEntityHandler */
    private PropertyEntityHandler $propertyEntityHandler;

    /**
     * AgentEntityHandler constructor.
     * @param EntityManagerInterface $entityManager
     * @param AgentRepository $agentRepository
     * @param PropertyEntityHandler $propertyEntityHandler
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        AgentRepository $agentRepository,
        PropertyEntityHandler $propertyEntityHandler
    ) {
        $this->entityManager = $entityManager;
        $this->agentRepository = $agentRepository;
        $this->propertyEntityHandler = $propertyEntityHandler;
    }

    /**
     * @param bool $useDynamicSeed
     * @return string
     */
    public function seedAgents(bool $useDynamicSeed = false): string
    {
        if (true === $useDynamicSeed) {
            $numberOfAgents = $this->getNumberOfAgents();
            $properties = $this->propertyEntityHandler->getAllProperties();

            try {
                foreach ($properties as $property) {
                    if (0 === random_int(0, 2)) {
                        $property->addAgent($this->getAgentById(random_int(1, $numberOfAgents)));
                        $this->entityManager->persist($property);
                    }
                }
                $this->entityManager->flush();
            } catch (Exception $exception) {
                return $exception->getMessage();
            }
        } else {
            $agentToPropertyIds = [
                0 => [1, 1], 1 => [1, 2], 2 => [1, 3], 3 => [2 ,2], 4 => [2 ,3], 5 => [3 ,1],
                6 => [3 ,3], 7 => [3 ,5], 8 => [4 ,3], 9 => [4 ,4], 10 => [4 ,6], 11 => [5 ,1],
                12 => [5 ,2], 13 => [5 ,5], 14 => [6 ,4], 15 => [6 ,6]
            ];

            try {
                foreach ($agentToPropertyIds as $index => $idArray) {
                    list ($agentId, $propertyId) = $idArray;
                    $property = $this->propertyEntityHandler
                        ->findPropertyByCriteria(['id' => $propertyId])
                        ->addAgent($this->getAgentById($agentId));
                    $this->entityManager->persist($property);
                }
                $this->entityManager->flush();

            } catch (Exception $exception) {
                return $exception->getMessage();
            }
        }

        return 'Done';
    }

    /**
     * @return int
     */
    public function getNumberOfAgents(): int
    {
        return $this->agentRepository->count([]);
    }

    /**
     * @param int $id
     * @return Agent|null
     */
    public function getAgentById(int $id): ?Agent
    {
        return $this->agentRepository->findOneBy(['id' => $id]);
    }

    /**
     * @return array|null
     */
    public function getAllAgents(): ?array
    {
        return $this->agentRepository->findAll();
    }
}

<?php
declare(strict_types=1);
/**
 * Entity handler for Property entity
 */

namespace App\EntityHandler;

use App\Entity\Property;
use App\Repository\PropertyRepository;

/**
 * Class PropertyEntityHandler
 * @package App\EntityHandler
 */
class PropertyEntityHandler
{
    /** @var PropertyRepository $propertyRepository */
    private PropertyRepository $propertyRepository;

    /**
     * PropertyEntityHandler constructor.
     * @param PropertyRepository $propertyRepository
     */
    public function __construct(PropertyRepository $propertyRepository)
    {
        $this->propertyRepository = $propertyRepository;
    }

    /**
     * @return array|null
     */
    public function getAllProperties(): ?array
    {
        return $this->propertyRepository->findAll();
    }

    /**
     * @param array $criteria
     * @return Property|null
     */
    public function findPropertyByCriteria(array $criteria): ?Property
    {
        return $this->propertyRepository->findOneBy($criteria);
    }
}
<?php
declare(strict_types=1);
/**
 * API Controller
 */

namespace App\Controller;

use App\EntityHandler\AgentEntityHandler;
use App\Service\ApiFunctions;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class Api
 * @package App\Controller
 */
class Api extends AbstractController
{
    /** @var AgentEntityHandler $agentEntityHandler */
    private AgentEntityHandler $agentEntityHandler;

    /** @var ApiFunctions $apiFunctions */
    private ApiFunctions $apiFunctions;

    /**
     * Api constructor.
     * @param AgentEntityHandler $agentEntityHandler
     * @param ApiFunctions $apiFunctions
     */
    public function __construct(AgentEntityHandler $agentEntityHandler,
                                ApiFunctions $apiFunctions)
    {
        $this->agentEntityHandler = $agentEntityHandler;
        $this->apiFunctions = $apiFunctions;
    }

    /**
     * @return Response
     * @Route ("/api/populate")
     */
    public function populateOrUpdateDB(): Response
    {
        return $this->json($this->apiFunctions->getData());
    }

    /**
     * @return Response
     * @Route("/api/seed")
     */
    public function applyAgents(): Response
    {
        return $this->json($this->agentEntityHandler->seedAgents());
    }

    /**
     * @param int $numberOfProperties
     * @param int $numberOfAgents
     * @return Response
     * @Route ("/api/topagents/{numberOfProperties}/{numberOfAgents}", defaults={"numberOfProperties"=2, "numberOfAgents"=2})
     */
    public function getTopAgents(int $numberOfProperties, int $numberOfAgents): Response
    {
        return $this->json($this->apiFunctions->getTopAgents($numberOfProperties, $numberOfAgents));
    }
}

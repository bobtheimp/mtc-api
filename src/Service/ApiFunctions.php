<?php
declare(strict_types=1);
/**
 * Entity handler for Agent entity
 */

namespace App\Service;

use App\Entity\Agent;
use App\Entity\Property;
use App\EntityHandler\AgentEntityHandler;
use App\EntityHandler\PropertyEntityHandler;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use HttpResponseException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Throwable;

/**
 * Class ApiFunctions
 * @package App\Service
 */
class ApiFunctions
{
    /** @var string $apiKey */
    private string $apiKey = '3NLTTNlXsi6rBWl7nYGluOdkl2htFHug';

    /** @var string $url */
    private string $url = 'https://trial.craig.mtcserver15.com/api/properties';

    /** @var HttpClientInterface $httpClient */
    private HttpClientInterface $httpClient;

    /** @var PropertyEntityHandler $propertyEntityHandler */
    private PropertyEntityHandler $propertyEntityHandler;

    /** @var AgentEntityHandler $agentEntityHandler */
    private AgentEntityHandler $agentEntityHandler;

    /** @var EntityManagerInterface $entityManager */
    private EntityManagerInterface $entityManager;

    /**
     * PopulateOrUpdateDB constructor.
     * @param HttpClientInterface $httpClient
     * @param PropertyEntityHandler $propertyEntityHandler
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        HttpClientInterface $httpClient,
        PropertyEntityHandler $propertyEntityHandler,
        AgentEntityHandler $agentEntityHandler,
        EntityManagerInterface $entityManager
    ){
        $this->httpClient = $httpClient;
        $this->propertyEntityHandler = $propertyEntityHandler;
        $this->agentEntityHandler = $agentEntityHandler;
        $this->entityManager = $entityManager;
    }

    /**
     * @return string
     */
    public function getData(): string
    {
        $pageNumber = $maxNumberPages = 1;

        do {
            try {
                $content = $this->getPage($pageNumber++);
                // when testing the API endpoint, the call would only return the first page, the pagenumber and
                // pagesize parameters appear to be ignored, so we end up with 400-odd pages of the same data!
                // so the next line is commented out until it's fixed!
                //$maxNumberPages = $content['last_page'];
                if (null !== $content && isset($content['data'])) {
                    foreach ($content['data'] as $page) {
                        $this->processPageData($page);
                    }
                }
            } catch (Throwable $exception) {
                // report the error!
                return $exception->getMessage();
            }
        } while ($pageNumber < $maxNumberPages);

        return "Done";
    }

    /**
     * @param int $page
     * @return array|null
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws DecodingExceptionInterface
     */
    private function getPage(int $page): ?array
    {
        $response = $this->httpClient->request(
            'GET',
            $this->url,
            ['headers' => [
                'Accept' => '*/*',
                'api_key' => $this->apiKey,
                'pagenumber' => $page,
                'pagesize' => 30
            ]]
        );

        return ($response instanceof ResponseInterface && 200 === $response->getStatusCode())
            ? $response->toArray()
            : null;
    }

    /**
     * @param array $pageData
     * @throws HttpResponseException
     * @throws Exception
     */
    private function processPageData(array $pageData): void
    {
        if (true === $this->checkPageDataValid($pageData)) {

            $property = $this->propertyEntityHandler->findPropertyByCriteria(['uuid' => $pageData['uuid']]);
            if (null === $property) {
                $property = new Property();
                $property->setUuid($pageData['uuid']);
            }
            $property->setCountry($pageData['country'])
                ->setDescription($pageData['description'])
                ->setForSaleForRent($pageData['type'])
                ->setLatitude($pageData['latitude'])
                ->setLongitude($pageData['longitude'])
                ->setNumberOfBathrooms((int)$pageData['num_bathrooms'])
                ->setNumberOfBedrooms((int)$pageData['num_bedrooms'])
                ->setPrice((int)$pageData['price'])
                ->setPropertyId((int)$pageData['property_type_id'])
                ->setPropertyType((int)$pageData['property_type_id'])
                ->setTown($pageData['town']);

            $this->entityManager->persist($property);
            $this->entityManager->flush();
        } else {
            throw new HttpResponseException('Invalid response from URL!');
        }
    }

    /**
     * @param array $pageData
     * @return bool
     */
    private function checkPageDataValid(array $pageData): bool
    {
        // we should validate the structure of the $pageData returned here .... * should *
        return null !== $pageData;
    }

    /**
     * @param int $numberOfProperties
     * @param int $numberOfOtherAgents
     * @return array
     */
    public function getTopAgents(int $numberOfProperties, int $numberOfOtherAgents): array
    {
        // TODO: optimise this, condense to an SQL query?
        $topAgents = [];
        $potentialTopAgent = [];

        $agents = $this->agentEntityHandler->getAllAgents();
        /** @var Agent $agent */
        foreach ($agents as $agent) {
            $agentCount = 1;
            foreach ($agents as $agentToSearch) {
                if ($agentToSearch->getName() === $agent->getName()) {
                    continue;
                }
                if (count(
                        array_intersect($agent->getPropertyIDs(), $agentToSearch->getPropertyIDs())
                    ) >= $numberOfProperties
                ) {
                    $potentialTopAgent[$agent->getName()] = $agentCount++;
                }
            }
        }

        foreach ($potentialTopAgent as $name => $agentsSharesPropertiesWith) {
            if ($agentsSharesPropertiesWith >= $numberOfOtherAgents) {
                $topAgents[$name] = $agentsSharesPropertiesWith;
            }
        }

        return $topAgents;
    }
}

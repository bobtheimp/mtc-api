<?php
declare(strict_types=1);
/**
 * Description:
 * Property entity
 */

namespace App\Entity;

use App\Entity\EntityTrait\ID;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Property
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\PropertyRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="property")
 */
class Property
{
    use ID;

    /**
     * Properties can have many agents
     * @ORM\ManyToMany(targetEntity="Agent", mappedBy="properties")
     * @ORM\JoinTable(name="properties_agents")
     */
    private Collection $agents;

    /** @ORM\Column(type="string", length="50", name="uuid") */
    private string $uuid;

    /** @ORM\Column(type="integer", name="property_id") */
    private int $property_id;

    /** @ORM\Column(type="text", length="65535", name="description") */
    private string $description;

    /** @ORM\Column(type="string", length="25", name="town") */
    private string $town;

    /** @ORM\Column(type="string", length="45", name="country") */
    private string $country;

    /** @ORM\Column(type="integer", name="property_type") */
    private int $property_type;

    /** @ORM\Column(type="integer", name="number_of_bedrooms") */
    private int $number_of_bedrooms;

    /** @ORM\Column(type="integer", name="number_of_bathrooms") */
    private int $number_of_bathrooms;

    /** @ORM\Column(type="integer", name="price") */
    private int $price;

    /** @ORM\Column(type="string", length="13", name="latitude") */
    private string $latitude;

    /** @ORM\Column(type="string", length="13", name="longitude") */
    private string $longitude;

    /** @ORM\Column(type="string", length="5", name="for_sale_or_rent") */
    private string $for_sale_for_rent;

    /**
     * Property constructor.
     */
    public function __construct()
    {
        $this->agents = new ArrayCollection();
    }

    /**
     * @return Collection|null
     */
    public function getAgents(): ?Collection
    {
        return $this->agents;
    }

    /**
     * @param Agent|null $agent
     * @return $this
     */
    public function addAgent(?Agent $agent): self
    {
        if (!$this->agents->contains($agent)) {
            $this->agents[] = $agent;
            $agent->addProperty($this);
        }

        return $this;
    }

    /**
     * @param Agent $agent
     * @return $this
     */
    public function removeAgent(Agent $agent): self
    {
        if ($this->agents->contains($agent)) {
            $this->agents->removeElement($agent);
            if ($agent->getProperties()->contains($this)) {
                $agent->removeProperty($this);
            }
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     * @return $this
     */
    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPropertyId(): ?int
    {
        return $this->property_id;
    }

    /**
     * @param int $property_id
     * @return $this
     */
    public function setPropertyId(int $property_id): self
    {
        $this->property_id = $property_id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTown(): ?string
    {
        return $this->town;
    }

    /**
     * @param string $town
     * @return $this
     */
    public function setTown(string $town): self
    {
        $this->town = $town;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return $this
     */
    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPropertyType(): ?int
    {
        return $this->property_type;
    }

    /**
     * @param int $property_type
     * @return $this
     */
    public function setPropertyType(int $property_type): self
    {
        $this->property_type = $property_type;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getNumberOfBedrooms(): ?int
    {
        return $this->number_of_bedrooms;
    }

    /**
     * @param int $number_of_bedrooms
     * @return $this
     */
    public function setNumberOfBedrooms(int $number_of_bedrooms): self
    {
        $this->number_of_bedrooms = $number_of_bedrooms;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getNumberOfBathrooms(): ?int
    {
        return $this->number_of_bathrooms;
    }

    /**
     * @param int $number_of_bathrooms
     * @return $this
     */
    public function setNumberOfBathrooms(int $number_of_bathrooms): self
    {
        $this->number_of_bathrooms = $number_of_bathrooms;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPrice(): ?int
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return $this
     */
    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     * @return $this
     */
    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     * @return $this
     */
    public function setLongitude(string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getForSaleForRent(): ?string
    {
        return $this->for_sale_for_rent;
    }

    /**
     * @param string $for_sale_for_rent
     * @return $this
     */
    public function setForSaleForRent(string $for_sale_for_rent): self
    {
        $this->for_sale_for_rent = $for_sale_for_rent;

        return $this;
    }
}
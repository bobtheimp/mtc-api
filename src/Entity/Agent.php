<?php
declare(strict_types=1);
/**
 * Description:
 * Agent entity
 */

namespace App\Entity;

use App\Entity\EntityTrait\ID;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Agent
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\AgentRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="agent")
 */
class Agent
{
    use ID;

    /** @ORM\Column(type="string", length="20", name="name") */
    private string $name;

    /**
     * Agents have Many Properties.
     * @ORM\ManyToMany(targetEntity="Property", inversedBy="agents")
     */
    private Collection $properties;

    /**
     * Agent constructor.
     */
    public function __construct()
    {
        $this->properties = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getProperties(): Collection
    {
        return $this->properties;
    }

    /**
     * @return array
     */
    public function getPropertyIDs(): array
    {
        $idArray = [];
        /** @var Property $property */
        foreach ($this->properties as $property) {
            $idArray[] = $property->getId();
        }
        return $idArray;
    }

    /**
     * @param Property $property
     * @return $this
     */
    public function addProperty(Property $property): self
    {
        if (!$this->properties->contains($property)) {
            $this->properties[] = $property;
            $property->addAgent($this);
        }

        return $this;
    }

    /**
     * @param Property $property
     * @return $this
     */
    public function removeProperty(Property $property): self
    {
        if ($this->properties->contains($property)) {
            $this->properties->removeElement($property);
            if ($this === $property->getAgents()) {
                $property->addAgent(null);
            }
        }

        return $this;
    }
}

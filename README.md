# MTC API #

This example is built in a docker container, using nginx latest, PHP7.4
and with Symfony framework 5.1.

### How do I get set up? ###

To run:

- Clone the repository
- Ensure docker-compose is installed and working
- Add docker.con to your hosts file set to 172.17.0.1 (or whatever docker is
  using as the bridge IP on your installation)
- Execute "docker-compose build && docker-compose up -d" in the docker
  directory
- execute the doctrine migrations in the php docker container
- Go to the URL: docker.con:8000/api/populate to add the property data to the database
- Go to the URL: docker.con:8000/api/seed to seed the agent data
- Go to the URL: docker.com:8000/api/topagents/2/2 to get the top agent data. The numerical arguments are the number of agents and number of properties respectively. If not supplied, the defaults of 2 and 2 will be used instead.

### Notes and improvements ###

The getTopAgents method of the ApiFunctions class is not optimal. Ideally I'd like to get it running as a simple SQL query, but it works.
There is an alternative agent seeding routine, but it is unused at the moment - could possibly adjust the seed api to use that instead of the default seeding. This was mainly for testing.

Also, I am aware that as of 5.2,
Sensio\Bundle\FrameworkExtraBundle\Routing\AnnotatedRouteControllerLoader
is deprecated, needs updating in the Kernel.php file, but at the moment
it's simply a runtime warning.